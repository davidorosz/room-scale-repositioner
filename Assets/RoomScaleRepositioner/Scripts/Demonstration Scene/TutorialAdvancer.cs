﻿using UnityEngine;
using RoomScaleRepositionerTool.Wand.Input;

public class TutorialAdvancer : MonoBehaviour
{
    [SerializeField]
    private GameObject startPoint;
    [SerializeField]
    private GameObject hiddenElements;
    [SerializeField]
    private GameObject cameraEye;
    [SerializeField]
    private Wand_InputDetector wandLeft;
    [SerializeField]
    private Wand_InputDetector wandRight;
    [SerializeField]
    private GameObject[] panels;

    private int currentPanel = 0;

    void Update()
    {
        if (wandLeft.IsUp(WandButton.Trigger) || wandRight.IsUp(WandButton.Trigger))
        {
            MoveAndRotateSceneToCameraEyeForFirstPanel();
            RevealScene();
            HideAnyButLastPanel();
            RevealNextPanel();
        }
    }

    private void MoveAndRotateSceneToCameraEyeForFirstPanel()
    {
        if (currentPanel == 0)
        {
            Vector3 headPosition = cameraEye.transform.position;
            Vector3 headRotation = cameraEye.transform.rotation.eulerAngles;
            Vector3 startPointEulerAngles = startPoint.transform.rotation.eulerAngles;
            startPoint.transform.position = new Vector3(headPosition.x, startPoint.transform.position.y, headPosition.z);
            startPoint.transform.rotation = Quaternion.Euler(startPointEulerAngles.x, headRotation.y, startPointEulerAngles.z);
        }
    }

    private void RevealScene()
    {
        if (!hiddenElements.activeSelf)
        {
            hiddenElements.SetActive(true);
        }
    }

    private void HideAnyButLastPanel()
    {
        if (currentPanel < (panels.Length - 1))
        {
            panels[currentPanel].SetActive(false);
        }
    }

    private void RevealNextPanel()
    {
        if (currentPanel < panels.Length - 1)
        {
            currentPanel += 1;
            panels[currentPanel].SetActive(true);
        }
    }
}
