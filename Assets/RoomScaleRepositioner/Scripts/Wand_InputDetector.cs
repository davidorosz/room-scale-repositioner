﻿using UnityEngine;

namespace RoomScaleRepositionerTool.Wand.Input
{
    public class Wand_InputDetector : MonoBehaviour
    {
        private SteamVR_TrackedObject trackedObject;
        private SteamVR_Controller.Device device;
        private Vector2 axisPos;
        private Vector2 triggerPos;

        void Start()
        {
            InitializeWand();
        }

        private void InitializeWand()
        {
            trackedObject = GetComponent<SteamVR_TrackedObject>();
            device = SteamVR_Controller.Input((int)trackedObject.index);
        }

        public bool IsDown(WandButton button)
        {
            bool result = false;

            if (device != null)
            {
                switch (button)
                {
                    case WandButton.ApplicationMenu:
                        result = ApplicationMenuDown();
                        break;
                    case WandButton.Grip:
                        result = GripDown();
                        break;
                    case WandButton.Touchpad:
                        result = TouchpadDown();
                        break;
                    case WandButton.Trigger:
                        result = TriggerDown();
                        break;
                    default:
                        break;
                }
            }

            return result;
        }

        public bool IsHeld(WandButton button)
        {
            bool result = false;

            if (device != null)
            {
                switch (button)
                {
                    case WandButton.ApplicationMenu:
                        result = ApplicationMenuHeld();
                        break;
                    case WandButton.Grip:
                        result = GripHeld();
                        break;
                    case WandButton.Touchpad:
                        result = TouchpadHeld();
                        break;
                    case WandButton.Trigger:
                        result = TriggerHeld();
                        break;
                    default:
                        break;
                }
            }

            return result;
        }

        public bool IsUp(WandButton button)
        {
            bool result = false;

            if (device != null)
            {
                switch (button)
                {
                    case WandButton.ApplicationMenu:
                        result = ApplicationMenuUp();
                        break;
                    case WandButton.Grip:
                        result = GripUp();
                        break;
                    case WandButton.Touchpad:
                        result = TouchpadUp();
                        break;
                    case WandButton.Trigger:
                        result = TriggerUp();
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        #region Controller Input Calls--------------------------------------------------------------------------------

        public Vector2 GetFigurePositionOnPad()
        {
            return device.GetAxis();
        }

        public bool TouchpadDown()
        {
            return device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad);
        }

        public bool TouchpadHeld()
        {
            return device.GetPress(SteamVR_Controller.ButtonMask.Touchpad);
        }

        public bool TouchpadUp()
        {
            return device.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad);
        }

        public bool GripDown()
        {
            return device.GetPressDown(SteamVR_Controller.ButtonMask.Grip);
        }

        public bool GripHeld()
        {
            return device.GetPress(SteamVR_Controller.ButtonMask.Grip);
        }

        public bool GripUp()
        {
            return (device.GetPressUp(SteamVR_Controller.ButtonMask.Grip));
        }

        public bool ApplicationMenuDown()
        {
            return device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu);
        }

        public bool ApplicationMenuHeld()
        {
            return device.GetPress(SteamVR_Controller.ButtonMask.ApplicationMenu);
        }

        public bool ApplicationMenuUp()
        {
            return device.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu);
        }

        public bool TriggerDown()
        {
            return device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger);
        }

        public bool TriggerHeld()
        {
            return device.GetPress(SteamVR_Controller.ButtonMask.Trigger);
        }

        public bool TriggerUp()
        {
            return device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger);
        }

        public float TriggerPosX()
        {
            Vector2 triggerPos = new Vector2(device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis1).x, 0);
            return triggerPos.x;
        }

        public bool SystemButtonDown()
        {
            return device.GetPressDown(SteamVR_Controller.ButtonMask.System);
        }

        public bool SystemButtonHeld()
        {
            return device.GetPress(SteamVR_Controller.ButtonMask.System);
        }

        public bool SystemButtonUp()
        {
            return device.GetPressUp(SteamVR_Controller.ButtonMask.System);
        }

        #endregion
    }
}