﻿using UnityEngine;
using RoomScaleRepositionerTool.BoundaryRenderer;
using RoomScaleRepositionerTool.Wand.Input;

namespace RoomScaleRepositionerTool.Repositioner
{
    public class RoomScaleRepositioner : MonoBehaviour
    {

        public ControlTypes OneButtonPressedControls = ControlTypes.Position;
        public ControlTypes TwoButtonsPressedControls = ControlTypes.RotationAndPosition;

        public WandButton wandLeftButton = WandButton.ApplicationMenu;
        public WandButton wandRightButton = WandButton.ApplicationMenu;

        public Color32 lensTintColor = new Color32(0, 0, 0, 208);

        [SerializeField]
        private Wand_InputDetector wandLeft;
        [SerializeField]
        private Wand_InputDetector wandRight;
        [SerializeField]
        private Transform cameraRig;
        [SerializeField]
        private Transform cameraEye;
        [SerializeField]
        private RoomScaleBoundaryRenderer boundaryRenderer;
        [SerializeField]
        private GameObject lensesTintingSphere;

        private static Vector3 currentHeadPos;
        private static Vector3 previousHeadPos;
        private static Quaternion previousRotation;
        private static Quaternion currentRotation;

        [Range(0, 2)]
        public static int isHeld = 0;
        private static Wand_InputDetector dominantWand = null;

        private void Start()
        {
            SetTintingSphereProperties();
            DeactivateTintingSphere();
        }

        private void SetTintingSphereProperties()
        {
            if (lensesTintingSphere != null)
            {
                Material m = lensesTintingSphere.GetComponent<MeshRenderer>().material;
                m.color = lensTintColor;
            }
        }

        private void DeactivateTintingSphere()
        {
            if (lensesTintingSphere != null)
            {
                lensesTintingSphere.SetActive(false);
            }
        }

        private void Update()
        {
            PerformLeftWandAction();
            PerformRightWandAction();
        }

        private void PerformLeftWandAction()
        {
            if (wandLeft.IsDown(wandLeftButton))
            {
                Down(wandLeft);
            }
            else if (wandLeft.IsHeld(wandLeftButton))
            {
                Held(wandLeft);
            }
            else if (wandLeft.IsUp(wandLeftButton))
            {
                Up(wandLeft);
            }
        }

        private void PerformRightWandAction()
        {
            if (wandRight.IsDown(wandRightButton))
            {
                Down(wandRight);
            }
            else if (wandRight.IsHeld(wandRightButton))
            {
                Held(wandRight);
            }
            else if (wandRight.IsUp(wandRightButton))
            {
                Up(wandRight);
            }
        }

        public void Down(Wand_InputDetector wand)
        {
            if (DominantWandExists() == false)
            {
                CreateDominantWand(wand);
                PerformRepositionerInitialSetup();
            }
            AddNewActiveWand();
        }

        private void CreateDominantWand(Wand_InputDetector wand)
        {
            if (dominantWand == null)
            {
                dominantWand = wand;
            }
        }

        private void PerformRepositionerInitialSetup()
        {
            RecordRigTransform();
            ActivateTintingSphere();
            ActivateBounds();
        }

        private void RecordRigTransform()
        {
            previousHeadPos = cameraEye.position;
            previousRotation = cameraEye.rotation;
        }

        private void ActivateTintingSphere()
        {
            if (lensesTintingSphere != null)
            {
                lensesTintingSphere.SetActive(true);
            }
        }

        private void ActivateBounds()
        {
            if (boundaryRenderer != null)
            {
                boundaryRenderer.gameObject.SetActive(true);
                boundaryRenderer.DrawBoundary();
            }
        }

        public bool DominantWandExists()
        {
            if (dominantWand == null)
            {
                return false;
            }
            return true;
        }

        private void AddNewActiveWand()
        {
            isHeld += 1;
        }

        public void Held(Wand_InputDetector wand)
        {
            SetDominantWandIfNoneExist(wand);

            if (dominantWand == wand)
            {
                AdjustRigPerWandSettings();
            }
        }

        private void SetDominantWandIfNoneExist(Wand_InputDetector wand)
        {
            if (DominantWandExists() == false)
            {
                dominantWand = wand;
            }
        }

        private void AdjustRigPerWandSettings()
        {
            if (isHeld == 2)
            {
                PerformRepositionForPressing(TwoButtonsPressedControls);
            }
            else
            {
                PerformRepositionForPressing(OneButtonPressedControls);
            }
        }

        private void PerformRepositionForPressing(ControlTypes setting)
        {
            if (setting == ControlTypes.Position)
            {
                RepositionRigPosition();
            }
            else if (setting == ControlTypes.RotationAndPosition)
            {
                RepositionRigRotation();
                RepositionRigPosition();
            }

            RecordRigTransform();
        }

        private void RepositionRigPosition()
        {
            currentHeadPos = cameraEye.position;
            Vector3 currentAndPreviousDifference;

            currentAndPreviousDifference = new Vector3(previousHeadPos.x - currentHeadPos.x, 0, previousHeadPos.z - currentHeadPos.z);

            cameraRig.position = new Vector3(cameraRig.position.x + currentAndPreviousDifference.x, cameraRig.position.y, cameraRig.position.z + currentAndPreviousDifference.z);
        }

        private void RepositionRigRotation()
        {
            Vector3 previousEular = previousRotation.eulerAngles;
            Vector3 currentEular = cameraEye.rotation.eulerAngles;
            Vector3 differenceEular = previousEular - currentEular;
            Quaternion rotationCorrection = cameraRig.rotation;

            rotationCorrection.eulerAngles = new Vector3(cameraRig.rotation.eulerAngles.x, cameraRig.rotation.eulerAngles.y + differenceEular.y, cameraRig.rotation.eulerAngles.z);

            cameraRig.rotation = rotationCorrection;
        }

        public void Up(Wand_InputDetector wand)
        {
            if (OneButtonBeingPressed())
            {
                TurnOffRepositioner();
            }
            RemoveActiveWand(wand);
        }

        private bool OneButtonBeingPressed()
        {
            if (isHeld == 1)
            {
                return true;
            }
            return false;
        }

        public void TurnOffRepositioner()
        {
            if (isHeld == 1)
            {
                DeactivateTintingSphere();
                DeactivateBounds();
            }
        }

        private void DeactivateBounds()
        {
            if (boundaryRenderer != null)
            {
                boundaryRenderer.gameObject.SetActive(false);
            }
        }

        private void RemoveActiveWand(Wand_InputDetector wand)
        {
            if (isHeld > 0)
            {
                isHeld -= 1;
            }

            if (dominantWand == wand)
            {
                dominantWand = null;
            }
        }
    }
}