Room Scale Repositioner for SteamVR - 1.0.0

Unity Version:  5.5.3f1
SteamVR Version:  1.2.1

::Summary::

The Room Scale Repositioner is a tool to help the user make minor adjustments to the position and rotation of their virtual space within their Play Area.



::Instructions::

From new scene:
1)  Import Room Scale Repositioner with SteamVR into the project
2)  (optional) Open the Repositioner Demonstration for a guided sample of the tool
3)  Create a new scene and add the [CameraRig] from the RoomScaleRepositioner/Prefabs folder
4)  Delete the previous "Main Camera" from the scene

From existing scene:

1)  Import Room Scale Repositioner with SteamVR into the project
2)  Add Component "RoomScaleRepositioner" to the [CameraRig]
3)  Add Component "Wand_InputDetector" to each controller ("Controller (left)" and "Controller (right)" by default)
4)  Drag each controller respectively to the "RoomScaleRepositioner" variables "Wand Left" and "Wand Right" in the "[CameraRig]" "RoomScaleRepositioner" component
5)  Drag the "[CameraRig]" to the"RoomScaleRepositioner" "Camera Rig" variable in the "[CameraRig]" "RoomScaleRepositioner" component
6)  Drag the "Camera (eye)" in the "[CameraRig]" "Camera (head)" object to the"RoomScaleRepositioner" "Camera Eye" variable in the "[CameraRig]" "RoomScaleRepositioner" component
7)  Add the "RoomBounds" prefab into the "[CameraRig]" (if the user wants the Room Boundary to draw in), then assign it to the "Boundary Renderer" variable in the "[CameraRig]" "RoomScaleRepositioner" component
8)  Add the "LensesTintingSphere" prefab to the "Camera (eye)" (if the user wants to make the scene tinted a different color when Repositioner is active), then add it to the "Lenses Tinting Sphere" variable in the "[CameraRig]" "RoomScaleRepositioner" component



::Contact::

Dave - davidoroszgamedesign@gmail.com